#!/bin/bash
#===============================================================================
#
#          FILE:  battery.sh
# 
#         USAGE:  ./battery.sh 
# 
#   DESCRIPTION:  Low battery system popup for i3 using notify-send
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  25/03/2019 22:02:32 CET
#      REVISION:  ---
#===============================================================================

#!/bin/bash

BATTINFO=`acpi -b`
if [[ `echo $BATTINFO | grep Discharging` && `echo $BATTINFO | cut -f 5 -d " "` < 00:45:00 ]] ; then
    DISPLAY=:0.0 /usr/bin/notify-send "Batterie très faible" "$BATTINFO"
fi
