#! /bin/bash

function is_openvpn_currently_running() {
  if [[ $(pgrep openvpn) == "" ]]; then
    echo false
  else
    echo true
  fi
}

for interface in $(ls /sys/class/net/ | grep -v wg0 | grep -v lo | grep -v tun0);
do
  if [[ $(cat /sys/class/net/$interface/carrier) = 1 ]]; then OnLine=1; fi
done
if ! [ $OnLine ]; then
#  echo "" > /dev/stderr;
 echo "Pas de connexion"
  exit;
 else
          ip=$(wget --header 'x-pm-appversion: Other' \
               --header 'x-pm-apiversion: 3' \
               --header 'Accept: application/vnd.protonmail.v1+json' \
               -o /dev/null \
               --timeout 6 --tries 1 -q -O - 'https://api.protonmail.ch/vpn/location' \
               | python -c 'import json; _ = open("/dev/stdin", "r").read(); print(json.loads(_)["IP"])' 2> /dev/null)
          if [[ "$(is_openvpn_currently_running)" == true ]]; then
            echo   $ip
            else
              echo   $ip
          fi
fi
