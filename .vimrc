set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#rc()

Bundle 'powerline/powerline', {'rtp': 'powerline/bindings/vim/'} 
Bundle 'snipMate'
Bundle 'nerdtree'
Bundle 'emmet'
Bundle 'tcomment'
Bundle 'syntastic'
Bundle 'tagbar'
 
filetype plugin on
set number
set ai
set showcmd
set t_Co=256
syntax on
set bg=light
colorscheme vitamins 
set mouse=a
 
set nocompatible
set laststatus=2
 
map <F4> :tabe ~/.vimrc<CR>
map <C-e> :tabe 
map ! :w<CR>
map <C-x> :NERDTreeToggle<CR>
map ; A;<ESC>
 
let g:tagbar_autofocus=1
map <C-t> :TagbarToggle<CR>
 
map <C-b> A<br /><ESC>
map <F3> :tabe ~/.vim/bundle/snipMate/snippets/c.snippets<CR>
 
let g:user_emmet_leader_key = '<C-y>'
 
set tabstop =4
set shiftwidth =4
set softtabstop =4
 
set hlsearch
au BufRead,BufNewFile *.md set filetype=markdown
set fileencoding=utf-8
 
let g:syntastic_c_include_dirs = [ '../../include', 'include' ]
 
map <F2> :set spell!<CR>
set spelllang=fr
hi SpellBad ctermfg=Red ctermbg=NONE
 
set foldmethod=syntax
set nofoldenable
set foldlevel=1

set guifont=Fira\ Code\ 10
